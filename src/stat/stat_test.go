package stat_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/dimasdanz/webfetcher/src/stat"
)

type StatsTest struct {
	suite.Suite
	stat stat.Stat
}

func TestStats(t *testing.T) {
	suite.Run(t, new(StatsTest))
}

func (s *StatsTest) SetupTest() {
	s.stat = stat.New("./testdata")
}

func (s *StatsTest) TestScanErrFileNotExists() {
	_, err := s.stat.Scan("notexists")
	s.Require().Error(err)
}

func (s *StatsTest) TestScanSuccessful() {
	res, err := s.stat.Scan("dummy.html")
	s.Require().NoError(err)

	expectedLastFetch, err := os.Stat("./testdata/dummy.html")
	s.Require().NoError(err)

	expected := stat.Metadata{
		Site:       "dummy.html",
		NumLinks:   3,
		ImageCount: 4,
		LastFetch:  expectedLastFetch.ModTime().UTC(),
	}
	s.Equal(expected, res)
}
