package stat

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	"emperror.dev/errors"
	"github.com/PuerkitoBio/goquery"
)

type Stat struct {
	directory string
}

func New(dir string) Stat {
	return Stat{
		directory: dir,
	}
}

type Metadata struct {
	Site       string
	NumLinks   int
	ImageCount int
	LastFetch  time.Time
}

func (s Stat) Scan(site string) (Metadata, error) {
	path := fmt.Sprintf("%s/%s", s.directory, site)
	path = filepath.Clean(path)
	file, err := os.Open(path)
	if err != nil {
		return Metadata{}, errors.WrapIff(err, "failed to open file `'%s'", path)
	}

	// goquery.NewDocumentFromReader uses html.Parse under the hood
	// it's a full compliant html 5 parser, which won't error for any byte stream
	// we already ensure that os.Open will be valid when it reached here
	doc, _ := goquery.NewDocumentFromReader(file)

	links := doc.Find("a")
	images := doc.Find("img")

	stat, _ := file.Stat()

	return Metadata{
		Site:       site,
		NumLinks:   len(links.Nodes),
		ImageCount: len(images.Nodes),
		LastFetch:  stat.ModTime().UTC(),
	}, nil
}
