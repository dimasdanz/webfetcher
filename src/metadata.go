package src

import (
	"context"
	"fmt"
	"log/slog"
	"strings"

	"emperror.dev/errors"
	"gitlab.com/dimasdanz/webfetcher/src/stat"
)

//go:generate mockgen -destination=mocks/stat.go -package=mocks . Stat
type Stat interface {
	Scan(site string) (stat.Metadata, error)
}

type MetadataExtractor struct {
	fetcher Fetcher
	stat    Stat
}

func NewMetadataExtractor(f Fetcher, s Stat) MetadataExtractor {
	return MetadataExtractor{
		fetcher: f,
		stat:    s,
	}
}

func (m MetadataExtractor) Run(ctx context.Context, noDownload bool, urls ...string) error {
	if len(urls) == 0 {
		return errors.New("urls cannot be empty")
	}

	sites, err := validateURLs(urls)
	if err != nil {
		return errors.WithStackIf(err)
	}

	if !noDownload {
		errFetch := m.fetcher.Run(ctx, urls...)
		if errFetch != nil {
			return errors.WithStackIf(errFetch)
		}
	}

	ch := make(chan ScanResult)
	for _, site := range sites {
		go m.scan(site, ch)
	}

	var errScan error
	metadatas := make([]stat.Metadata, 0, len(sites))
	for range sites {
		res := <-ch
		if res.err != nil {
			errScan = errors.Append(errScan, res.err)

			continue
		}

		metadatas = append(metadatas, res.metadata)
	}

	if len(metadatas) > 0 {
		m.printMetadata(metadatas)
	}

	if errScan != nil {
		slog.Error("there were some errors happened during scanning")
		printError("extracting metadata", errScan)
	}

	return errScan
}

type ScanResult struct {
	metadata stat.Metadata
	err      error
}

func (m MetadataExtractor) scan(site Site, ch chan<- ScanResult) {
	metadata, errSc := m.stat.Scan(site.Name)
	ch <- ScanResult{
		metadata: metadata,
		err:      errSc,
	}
}

func (m MetadataExtractor) printMetadata(metadatas []stat.Metadata) {
	messages := make([]string, 0, len(metadatas))
	for _, metadata := range metadatas {
		meta := make([]string, 0, 4)
		meta = append(meta, fmt.Sprintf("site: %s", metadata.Site))
		meta = append(meta, fmt.Sprintf("num_links: %d", metadata.NumLinks))
		meta = append(meta, fmt.Sprintf("images: %d", metadata.ImageCount))
		meta = append(meta, fmt.Sprintf("last_fetch: %s", metadata.LastFetch.Format("Mon Jan 02 2006 15:04 MST")))

		messages = append(messages, strings.Join(meta, "\n"))
	}

	msg := fmt.Sprintf("Extracted metadata\n%s", strings.Join(messages, "\n\n"))
	slog.Info(msg)
}
