package src

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ValidateTest struct {
	suite.Suite
}

func TestValidate(t *testing.T) {
	suite.Run(t, new(ValidateTest))
}

func (s *ValidateTest) TestValidateURLReturnError() {
	tests := []struct {
		url             string
		expectedMessage string
	}{
		{
			url:             "",
			expectedMessage: "empty url",
		},
		{
			url:             "\n",
			expectedMessage: "invalid control character in URL",
		},
		{
			url:             "foobar",
			expectedMessage: "scheme is required",
		},
		{
			url:             "https://",
			expectedMessage: "host is required",
		},
	}

	for _, tt := range tests {
		s.Run(tt.expectedMessage, func() {
			_, err := validateURL(tt.url)
			s.Require().Error(err)
			s.Contains(err.Error(), tt.expectedMessage)
		})
	}
}

func (s *ValidateTest) TestValidateURLSuccess() {
	tests := []struct {
		url      string
		expected Site
	}{
		{
			url: "https://localhost",
			expected: Site{
				Name: "localhost.html",
				URL:  "https://localhost",
			},
		},
		{
			url: "https://google.com",
			expected: Site{
				Name: "google.com.html",
				URL:  "https://google.com",
			},
		},
		{
			url: "https://www.youtube.com",
			expected: Site{
				Name: "www.youtube.com.html",
				URL:  "https://www.youtube.com",
			},
		},
		{
			url: "https://www.youtube.com/channel/something",
			expected: Site{
				Name: "www.youtube.com_channel_something.html",
				URL:  "https://www.youtube.com/channel/something",
			},
		},
	}

	for _, tt := range tests {
		s.Run(tt.url, func() {
			res, err := validateURL(tt.url)
			s.Require().NoError(err)
			s.Equal(tt.expected, res)
		})
	}
}

func (s *ValidateTest) TestValidateURLs() {
	s.Run("error", func() {
		_, err := validateURLs([]string{"", "foobar"})
		s.Require().Error(err)
	})

	s.Run("success", func() {
		res, err := validateURLs([]string{"https://google.com", "https://dimasdanz.com"})
		s.Require().NoError(err)
		s.Equal(Site{
			Name: "google.com.html",
			URL:  "https://google.com",
		}, res[0])

		s.Equal(Site{
			Name: "dimasdanz.com.html",
			URL:  "https://dimasdanz.com",
		}, res[1])
	})
}
