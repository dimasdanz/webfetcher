package src

import (
	"context"
	"fmt"
	"log/slog"

	"emperror.dev/errors"
)

//go:generate mockgen -destination=mocks/downloader.go -package=mocks . Downloader
type Downloader interface {
	Download(ctx context.Context, url string) ([]byte, error)
}

//go:generate mockgen -destination=mocks/writer.go -package=mocks . Writer
type Writer interface {
	Write(site string, content []byte) error
}

type Fetcher struct {
	downloader Downloader
	writer     Writer
}

func NewFetcher(d Downloader, w Writer) Fetcher {
	return Fetcher{
		downloader: d,
		writer:     w,
	}
}

func (f Fetcher) Run(ctx context.Context, urls ...string) error {
	if len(urls) == 0 {
		return errors.New("urls cannot be empty")
	}

	sites, err := validateURLs(urls)
	if err != nil {
		printError("validating URLs", err)

		return errors.WithStackIf(err)
	}

	slog.Info(fmt.Sprintf("Will download %d sites", len(urls)))

	// if sites length will be in the millions range it might be better to chunk it?
	ch := make(chan error)
	for _, site := range sites {
		go f.downloadAndWrite(ctx, site, ch)
	}

	var errDownload error
	var errWrite error
	for _, site := range sites {
		errCh := <-ch
		if errors.Is(errCh, ErrDownload) {
			errs := errors.GetErrors(errCh)
			errDownload = errors.Append(errDownload, errors.Combine(errs[1:]...)) // remove constant error for cleaner message
			continue
		}

		if errors.Is(errCh, ErrWrite) {
			errs := errors.GetErrors(errCh)
			errWrite = errors.Append(errWrite, errors.Combine(errs[1:]...)) // remove constant error for cleaner message
			continue
		}

		slog.Info(fmt.Sprintf("Sites %s downloaded", site.Name))
	}

	if errDownload != nil {
		printError("downloading", errDownload)
	}

	if errWrite != nil {
		printError("writing", errWrite)
	}

	return errors.Combine(errDownload, errWrite)
}

const (
	ErrDownload = errors.Sentinel("ErrDownload")
	ErrWrite    = errors.Sentinel("ErrWrite")
)

func (f Fetcher) downloadAndWrite(ctx context.Context, site Site, ch chan<- error) {
	content, errDl := f.downloader.Download(ctx, site.URL)
	if errDl != nil {
		err := errors.Combine(ErrDownload, errDl)
		ch <- err

		return
	}

	errWr := f.writer.Write(site.Name, content)
	if errWr != nil {
		ch <- errors.Combine(ErrWrite, errWr)

		return
	}

	ch <- nil
}
