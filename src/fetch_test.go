package src

import (
	"context"
	"testing"

	"emperror.dev/errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dimasdanz/webfetcher/src/mocks"
)

type FetchTest struct {
	suite.Suite
	fetch      Fetcher
	downloader *mocks.MockDownloader
	writer     *mocks.MockWriter
}

func TestFetch(t *testing.T) {
	suite.Run(t, new(FetchTest))
}

func (s *FetchTest) SetupTest() {
	c := gomock.NewController(s.T())

	s.downloader = mocks.NewMockDownloader(c)
	s.writer = mocks.NewMockWriter(c)
	s.fetch = NewFetcher(s.downloader, s.writer)
}

func (s *FetchTest) TestRunErrEmpty() {
	err := s.fetch.Run(context.Background())
	s.Require().Error(err)
}

func (s *FetchTest) TestRunErrInvalidURL() {
	err := s.fetch.Run(context.Background(), "foo", "bar")
	s.Require().Error(err)
}

func (s *FetchTest) TestRunErrDownload() {
	s.downloader.EXPECT().Download(gomock.Any(), gomock.Any()).Return(nil, errors.New("something"))
	err := s.fetch.Run(context.Background(), "https://localhost")
	s.Require().Error(err)
}

func (s *FetchTest) TestRunErrWrite() {
	s.downloader.EXPECT().Download(gomock.Any(), gomock.Any()).Return([]byte(`something`), nil)
	s.writer.EXPECT().Write(gomock.Any(), gomock.Any()).Return(errors.New("something"))

	err := s.fetch.Run(context.Background(), "https://localhost")
	s.Require().Error(err)
}

func (s *FetchTest) TestRunSuccess() {
	s.downloader.EXPECT().Download(gomock.Any(), gomock.Any()).Return([]byte(`something`), nil).Times(2)
	s.writer.EXPECT().Write(gomock.Any(), gomock.Any()).Return(nil).Times(2)

	err := s.fetch.Run(context.Background(), "https://localhost", "https://google.com")
	s.Require().NoError(err)
}
