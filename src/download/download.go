package download

import (
	"context"
	"io"
	"net/http"

	"emperror.dev/errors"
)

//go:generate mockgen -destination=mocks/client.go -package=mocks . Client
type Client interface {
	Do(req *http.Request) (*http.Response, error)
}

type Downloader struct {
	client Client
}

func New(client Client) Downloader {
	return Downloader{client: client}
}

func (d Downloader) Download(ctx context.Context, url string) ([]byte, error) {
	req, _ := http.NewRequestWithContext(ctx, http.MethodGet, url, http.NoBody)

	res, err := d.client.Do(req)
	if err != nil {
		return nil, errors.WrapIff(err, "failed to download %s", url)
	}
	defer func() { _ = res.Body.Close() }()

	content, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, errors.WrapIff(err, "failed to read content of %s", url)
	}

	// TODO: reject if not html based on Content-Type header?
	// if res.Header.Get("Content-Type") != "text/html" {}

	return content, nil
}
