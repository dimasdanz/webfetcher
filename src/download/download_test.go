package download_test

import (
	"context"
	"io"
	"net/http"
	"strings"
	"testing"

	"emperror.dev/errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dimasdanz/webfetcher/src/download"
	"gitlab.com/dimasdanz/webfetcher/src/download/mocks"
)

type bodyReaderError struct{}

func (bodyReaderError) Read(_ []byte) (n int, err error) {
	return 0, errors.New("read error")
}

type DownloaderTest struct {
	suite.Suite
	client   *mocks.MockClient
	download download.Downloader
}

func TestDownloader(t *testing.T) {
	suite.Run(t, new(DownloaderTest))
}

func (s *DownloaderTest) SetupTest() {
	c := gomock.NewController(s.T())

	s.client = mocks.NewMockClient(c)
	s.download = download.New(s.client)
}

func (s *DownloaderTest) TestDownloadErrDo() {
	s.client.EXPECT().Do(gomock.Any()).Return(nil, errors.New("something"))

	_, err := s.download.Download(context.Background(), "thisisatest")
	s.Require().Error(err)
}

func (s *DownloaderTest) TestDownloadErrReadAll() {
	s.client.EXPECT().Do(gomock.Any()).Return(&http.Response{
		Body: io.NopCloser(bodyReaderError{}),
	}, nil)

	_, err := s.download.Download(context.Background(), "thisisatest")
	s.Require().Error(err)
}

func (s *DownloaderTest) TestDownloadSuccess() {
	s.client.EXPECT().Do(gomock.Any()).Return(&http.Response{
		Header: map[string][]string{"Content-Type": {"text/html", "charset=utf-8"}},
		Body:   io.NopCloser(strings.NewReader(`<html><head><title>Google</title></head><body><h1>Hi!</h1></body></html>`)),
	}, nil)

	content, err := s.download.Download(context.Background(), "thisisatest")
	s.Require().NoError(err)
	s.Contains(string(content), "Google")
}
