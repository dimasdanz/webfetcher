package src

import (
	"fmt"
	"log/slog"
	"net/url"
	"strings"

	"emperror.dev/errors"
)

type Site struct {
	// Name is used for filename when being saved and scanned
	Name string

	// URL is used to download the actual site
	URL string
}

func validateURLs(urls []string) ([]Site, error) {
	sites := make([]Site, 0, len(urls))
	var errs error
	for _, v := range urls {
		site, err := validateURL(v)
		if err != nil {
			errs = errors.Append(errs, errors.WrapIff(err, "'%s' URL is invalid", v))
			continue
		}

		sites = append(sites, site)
	}

	return sites, errs
}

func validateURL(val string) (Site, error) {
	var errs error

	if val == "" {
		errs = errors.Append(errs, errors.New("empty url"))
	}

	res, err := url.Parse(val)
	if err != nil {
		errs = errors.Append(errs, err)
	}

	if err == nil && res.Scheme == "" {
		errs = errors.Append(errs, errors.New("scheme is required"))
	}

	if err == nil && res.Host == "" {
		errs = errors.Append(errs, errors.New("host is required"))
	}

	if errs != nil {
		return Site{}, errs
	}

	path := strings.ReplaceAll(res.Path, "/", "_")
	return Site{
		Name: fmt.Sprintf("%s%s.%s", res.Host, path, "html"), // we're supporting HTML only for now
		URL:  val,
	}, nil
}

func printError(title string, err error) {
	errs := errors.GetErrors(err)

	message := fmt.Sprintf("Error occurred while %s\n", title)
	for _, e := range errs {
		message += fmt.Sprintf("%s\n", e.Error())
	}

	slog.Error(message)
}
