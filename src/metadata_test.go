package src

import (
	"context"
	"testing"

	"emperror.dev/errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/dimasdanz/webfetcher/src/mocks"
	"gitlab.com/dimasdanz/webfetcher/src/stat"
)

type MetadataExtractorTest struct {
	suite.Suite
	downloader        *mocks.MockDownloader
	writer            *mocks.MockWriter
	stat              *mocks.MockStat
	fetch             Fetcher
	metadataExtractor MetadataExtractor
}

func TestMetadataExtractor(t *testing.T) {
	suite.Run(t, new(MetadataExtractorTest))
}

func (s *MetadataExtractorTest) SetupTest() {
	c := gomock.NewController(s.T())

	s.downloader = mocks.NewMockDownloader(c)
	s.writer = mocks.NewMockWriter(c)
	s.fetch = NewFetcher(s.downloader, s.writer)
	s.stat = mocks.NewMockStat(c)
	s.metadataExtractor = NewMetadataExtractor(s.fetch, s.stat)
}

func (s *MetadataExtractorTest) TestRunErrEmpty() {
	err := s.metadataExtractor.Run(context.Background(), false)
	s.Require().Error(err)
}

func (s *MetadataExtractorTest) TestRunErrInvalidURL() {
	err := s.metadataExtractor.Run(context.Background(), false, "foo", "bar")
	s.Require().Error(err)
}

func (s *MetadataExtractorTest) TestRunErrFetcher() {
	s.downloader.EXPECT().Download(gomock.Any(), gomock.Any()).Return(nil, errors.New("something"))
	err := s.metadataExtractor.Run(context.Background(), false, "https://localhost")
	s.Require().Error(err)
}

func (s *MetadataExtractorTest) TestRunErrScan() {
	s.downloader.EXPECT().Download(gomock.Any(), gomock.Any()).Return([]byte(`something`), nil)
	s.writer.EXPECT().Write(gomock.Any(), gomock.Any()).Return(nil)
	s.stat.EXPECT().Scan(gomock.Any()).Return(stat.Metadata{}, errors.New("something"))

	err := s.metadataExtractor.Run(context.Background(), false, "https://localhost")
	s.Require().Error(err)
}

func (s *MetadataExtractorTest) TestRunSuccess() {
	s.downloader.EXPECT().Download(gomock.Any(), gomock.Any()).Return([]byte(`something`), nil)
	s.writer.EXPECT().Write(gomock.Any(), gomock.Any()).Return(nil)
	s.stat.EXPECT().Scan(gomock.Any()).Return(stat.Metadata{}, nil)

	err := s.metadataExtractor.Run(context.Background(), false, "https://localhost")
	s.Require().NoError(err)
}

func (s *MetadataExtractorTest) TestRunSuccessNoDownload() {
	s.stat.EXPECT().Scan(gomock.Any()).Return(stat.Metadata{}, nil)

	err := s.metadataExtractor.Run(context.Background(), true, "https://localhost")
	s.Require().NoError(err)
}
