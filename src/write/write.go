package write

import (
	"fmt"
	"os"
	"path/filepath"

	"emperror.dev/errors"
)

type Writer struct {
	output string
}

func New(output string) Writer {
	return Writer{
		output: output,
	}
}

func (w Writer) Write(site string, content []byte) error {
	path := fmt.Sprintf("%s/%s", w.output, site)
	path = filepath.Clean(path)

	err := os.WriteFile(path, content, 0o600)
	if err != nil {
		return errors.WrapIff(err, "failed to write %s", path)
	}

	return nil
}
