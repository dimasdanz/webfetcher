package write_test

import (
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/dimasdanz/webfetcher/src/write"
)

type WriterTest struct {
	suite.Suite
	writer write.Writer
	output string
}

func TestWriter(t *testing.T) {
	suite.Run(t, new(WriterTest))
}

func (s *WriterTest) SetupTest() {
	s.output = s.T().TempDir()
	s.writer = write.New(s.output)
}

// testWriterErr disabling this test as running this in alpine
// does not make the temp directory readonly
func (s *WriterTest) testWriterErr() { //nolint:unused
	// create a non-writable directory in TempDir
	path := filepath.Join(s.output, "nonwritable")
	_ = os.Mkdir(path, 0o400)

	err := write.New(path).Write("localhost.html", []byte("content"))
	s.Require().Error(err)
}

func (s *WriterTest) TestWriterSuccess() {
	err := s.writer.Write("localhost.html", []byte("content"))
	s.Require().NoError(err)

	// validate file exists
	path := filepath.Join(s.output, "localhost.html")
	f, err := os.Open(path)
	s.Require().NoError(err)
	defer func() { _ = f.Close() }()

	// validate content equal
	content, err := io.ReadAll(f)
	s.Require().NoError(err)
	s.Equal("content", string(content))
}
