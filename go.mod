module gitlab.com/dimasdanz/webfetcher

go 1.21

require (
	emperror.dev/errors v0.8.1
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/golang/mock v1.6.0
	github.com/spf13/cobra v1.8.0
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
