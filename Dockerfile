FROM golang:1.21-alpine AS builder

WORKDIR /app

COPY . ./

# git, gcc, and libc-dev are required for running `go build`
# global git config for pulling private packages
RUN apk add --no-cache git gcc libc-dev \
    && go build -o webfetcher

FROM alpine AS runner

WORKDIR /app

# tzdata is required for time.LoadLocation to work properly
RUN apk add --no-cache tzdata

# copy binary from builder
COPY --from=builder /app/webfetcher .

ENTRYPOINT ["/app/webfetcher"]
