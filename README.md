WebFetcher
---
Built with ❤️

## Build

If you have Golang 1.21 setup locally, do a build like this
```bash
make build
./webfetcher
```

Alternatively, you can build Dockerfile and run it
```bash
docker build -t webfetcher .
docker run --rm webfetcher
```

## Usage

Generally the command look like this. You can run `--help` to print more detail
```bash
./webfetcher url1 url2 url3 ...
```

There are three modes in webfetcher
1. Download HTML file only
    ```bash
    `./webfetcher https://google.com https://dimasdanz.com`
    ```
2. Download HTML file and extract the metadata:
    ```bash
    `./webfetcher --metadata https://google.com https://dimasdanz.com`
    ```

3. Extract the metadata of an already downloaded sites
    ```bash
    `./webfetcher --metadata --no-download https://google.com https://dimasdanz.com`
    ```

## Roadmap

In the future, we would like to have a local mirror of all
the downloaded sites so that saved pages can be navigated
without any internet connection.

In this case, we would traverse all the html and download
all assets into local mimicking all the path and replace
all references with local file

## Contributing

To contribute, ensure that you can the following command
before making changes into the code
```bash
make lint # this will run golangci-lint ensuring formatting is correct
make test # this will run unit test to all files in this project
```

Above command is also being executed as a GitLab pipeline
on every push and merge request. Failure pipeline cannot be merged 
