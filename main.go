package main

import (
	"log/slog"
	"net/http"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/dimasdanz/webfetcher/src"
	"gitlab.com/dimasdanz/webfetcher/src/download"
	"gitlab.com/dimasdanz/webfetcher/src/stat"
	"gitlab.com/dimasdanz/webfetcher/src/write"
)

func main() {
	// force all time to UTC. convert to other timezone when necessary
	_ = os.Setenv("TZ", "UTC")

	var outputDir string
	var metadataFlag bool
	var noDownload bool
	cmd := &cobra.Command{
		Use:           "./webfetcher [--metadata][--output] urls ...",
		Short:         "WebFetcher",
		SilenceErrors: true,
		SilenceUsage:  true,
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				return cmd.Help()
			}

			fetcher := buildFetcher(outputDir)
			if metadataFlag {
				metadataExtractor := buildMetadataExtractor(fetcher, outputDir)

				return metadataExtractor.Run(cmd.Context(), noDownload, args...)
			}

			return fetcher.Run(cmd.Context(), args...)
		},
	}
	cmd.Flags().StringVarP(&outputDir, "output", "o", ".",
		"Set output directory. Default to current directory",
	)
	cmd.Flags().BoolVarP(&metadataFlag, "metadata", "m", false,
		"Print metadata. Will download before printing the metadata unless --no-download is specified",
	)
	cmd.Flags().BoolVarP(&noDownload, "no-download", "n", false,
		"Do not download if site is not exists. Only works if --metadata flag is passed",
	)

	if err := cmd.Execute(); err != nil {
		slog.Error("got error when executing command")

		os.Exit(1)
	}
}

func buildFetcher(output string) src.Fetcher {
	client := &http.Client{
		Timeout: 30 * time.Second,
	}
	downloader := download.New(client)
	writer := write.New(output)

	return src.NewFetcher(downloader, writer)
}

func buildMetadataExtractor(fetcher src.Fetcher, directory string) src.MetadataExtractor {
	st := stat.New(directory)
	return src.NewMetadataExtractor(fetcher, st)
}
