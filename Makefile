lint:
	golangci-lint run

test:
	go test -coverprofile coverage.out -race ./...
	go tool cover -func coverage.out

build:
	go build -o webfetcher
